#   Copyright 2018-2020 The University of Birmingham
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.

#!/bin/bash
##! if called with -h or, -help or, --help or, -H optiins print help message and exit
if [ "$1" == "-h" ] || [ "$1" == "-help" ] || [ "$1" == "--help" ] || [ "$1" == "-H" ]; then
  echo
  echo " Hog - .pages Creator"
  echo " ---------------------------"
  echo " Creates a .pages file for the mkdocs awesome page plugin"
  echo " Ordering all the files in a folder from the newest to the oldest"
  echo
  echo "Usage:"
  echo -e "\t ./pages_creator <folder> <title>\n"
  echo
  exit 0
fi

##! If no arg is provided Then print usage message
if [ -z "$1" ] || [ -z "$2" ]
then
	printf "Folder or title names have not been specified. Usage: \n ./pages_creator <folder> <title>\n"
else
	folder=$1
	title=$2
	echo "title: \"$title\"" > $folder/.pages
  echo "arrange:" >> $folder/.pages
	for file in `ls -r $folder/*.md`; do
		echo "    - ${file##*/}" >> $folder/.pages
	done
fi
