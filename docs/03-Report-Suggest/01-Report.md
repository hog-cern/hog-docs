<!-- # Bug Report and Suggestions -->

# Report a bug

You can report bugs and suggest new features opening issues on Hog's GitLab repository: <https://gitlab.com/hog-cern/Hog/issues>

Please report bugs one at the time, and before doing so check if the same bug has been already reported [here](https://gitlab.com/hog-cern/Hog/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Problem%20Report)

Open an issue using the label ![problem report](./figures/problem.png)

In the [Description] section of the [Issue] try to describe in detail the problem that you encountered.

Let us know:

- error/unexpected message you receive
- expected vs actual result
- if the bug is reproducible and how
- which Hog version you are using
- if you are working on Windows or Linux
- if you are using Vivado, ISE, or Quartus and which version
- the link to your repository (or to where your code is online stored)

If you want to ask a question about Hog rather than report a bug, you can use the label ![question](./figures/question.png)

Please do not use any other labels apart form "Feature proposal", "Question" and "Problem report" because they are meant for developers rather than users.
