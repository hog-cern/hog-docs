## Hog Versioning Scheme
Hog uses a semantic version scheme of the form `M.m.p`. Where `M` is the major version, `m` is the minor version and `p` is the *patch*. Hog automatically increases the version of your projects by means of specially named git branches.

The version of your project is stored in specific *annotated Tags* of the from `vM.m.p`, e.g. `v1.2.3`. 
:::{warning}
You can create any tag on the repository, light-weighted or annotated. The only exception are *annotated tags* like `v1.3.13`. These tags will be interpreted by Hog as a repository version.
:::

When you build your project, the pre-synthesis script parses the tags, finds out the version of your project and passes it as a generic to you top file. To be precise, Hog does this for any subset of files specified in the project's source files. This is why there is a parameter/generic for the SHA and the version for every library in the project.

:::{note}
As the version is fed to the design to be hardcoded in registers, *the version number must be known before synthesis starts*. This is a fundamental difference with respect to software, where you can decide the version number after the new feature is merged to the release branch.
:::

### Increasing the version number
But how to increase the version number? You can do it manually, creating an annotated tag of the form `vM.m.p`, but it's much better to let Hog CI do it automatically for you.

Hog's default behaviour is to increase the value of `p`. So if you don't do anything about it, your version will be `v0.0.1`, `v0.0.2`, and so on up to `v0.0.65535` (there are 2 bytes available for `p` in the registers).

If you want to increase `m` or `M`, you need to use special branch names for your merge request.
By default, if the name of your branch starts with `major_version/`, Hog will increase the major number `M`. If it starts with `minor_version/`, it will increase the minor number `m`, and it increases the patch number `p` in all other cases.

This behaviour can customised with the configuration file `Top/repo.conf`. Here are the possible configurations:

```ini
[main]
ENABLE_DEVELOP_BRANCH = 0

[prefixes]
HOTFIX = hotfix/
MINOR_VERSION = minor_version/
MAJOR_VERSION = major_version/
```

### Working with develop and master branches
By setting the variable `ENABLE_DEVELOP_BRANCH` to 1, you will tell Hog to increase by default the minor number `m`, rather than `p`.

This feature is meant for repositories, which work using a two-release-branch approach, where you have a *develop* branch, where the pull/merge request are directed, and a *master* branch, which represent the stable version of your project.

Updating the *develop* branch means that a new feature was added so `m` is increased. Whereas updating the master branch directly (i.e. not merging *develop* to *master* but make a modification starting from *master* back into master), means that a hotfix has been added, resulting in an increased patch number `p`.

So when you plan to start from master and merge back to it, you should use the *hotfix* prefix in your branch name (`hotfix/` by default). This will tell hog to increase the patch number, rather than the minor version. 

Even if it is not necessarily true that every time you commit to *develop* you are adding a new feature, this simple scheme avoids duplicated versions.

In this scheme, when merging from the develop branch to the master branch, the Hog-CI will not run any pipeline, and will not increase any version number, since these changes have been already validated by the CI. To use a develop branch, developers have to specify the `HOG_DEVELOP_BRANCH` variable as explained in the [GitLab environmental variable](03-GitLab-CI/01-setup-CI.md#environment-variables) and [GitHub secrets and inputs](04-GitHub.md#secrets-and-inputs) sections. 

:::{note}
As explained above, it is also possible to change the default branch prefixes, which tell Hog which version number has to be updated. 
:::

### Examples
Here are few examples of version number increasing:

#### With one-release-branch scheme
Let's suppose that the last tag of your design is v1.2.4, the possible scenarios are:

| Branch name        | Original version | Final version |
|:---------------------------------|:----------------:|:-------------:|
| starts with `minor_version/`     | v1.2.4           | v1.3.0        |
| starts with `major_version/`     | v1.2.4           | v2.0.0        |
| any other case                   | v1.2.4           | v1.2.5        |

#### With two-release-branch scheme: develop and master
Let's suppose again that the last tag of your design is v1.2.0, the possible scenarios are:

| Branch name        | Original version | Final version |
|:---------------------------------|:----------------:|:-------------:|
| starts with `hotfix/`            | v1.2.0           | v1.2.1        |
| starts with `major_version/`     | v1.2.0           | v2.0.0        |
| any other case                   | v1.2.0           | v1.3.0        |
