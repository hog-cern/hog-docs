## Setting up a Virtual Machines for Gateware implementation
To run the Hog CI, you need a dedicated machine which will run a private runner, since the available shared runners on GitLab or GitHub are not ideal to execute heavy software like Vivado or Quartus.

To setup the private runner refers to the official documentations:

- [Use self-managed runners for GitLab CI/CD](https://docs.gitlab.com/runner/#use-self-managed-runners).
- [Adding self-hosted runners for GitHub Actions](https://docs.github.com/en/actions/hosting-your-own-runners/managing-self-hosted-runners/adding-self-hosted-runners#prerequisites).

Once you have setup your machine for the chosen environment, refer to the [GitLab](03-GitLab-CI/01-setup-CI.md#set-up-runners) or [GitHub](04-GitHub.md#set-up-runners) sections on how to configure your repository to run the Hog-CI on the created runners. 


### Create a CERN Openstack Virtual Machine

If you work at CERN, you can create a Virtual Machine using the OpenStack framework at [openstack.cern.ch](https://openstack.cern.ch). We suggest to create a machine based on Alma9, for the best compatibility with the latest version of the IDE and git software. 

Instructions on how to setup the openstack virtual machine can be find in the [official documentation hub](https://clouddocs.web.cern.ch/index.html).

Once your machine is setup, install the required software (Vivado, QuestaSim, Quartus, Libero). 

Follow the instructions to install them from the correspondent vendor website.

```{warning}
You are the one responsible for correctly licensing the software.
```



