## Hog-CI with containers

It is also possible to run the Hog-CI inside [Apptainer](http://apptainer.org/) or [Docker](https://www.docker.com/) containers. The following configurations must be implemented for this purpose.

### Hog-CI with Apptainer 

To run the Hog-CI with Apptainer container, you need a Virtual Machine with Apptainer installed and with access to your Apptainer `.sif` container file, whose location must be set with the `HOG_APPTAINER_IMAGE` environmental variable. Refer to the following the instructions on how to setup environmental variables [for GitLab CI/CD](03-GitLab-CI/01-setup-CI.md#environment-variables) and [for GitHub Actions](04-GitHub.md#secrets-and-inputs). 

You can also pass a custom path to your container, which will then be accessible during execution, by setting the `HOG_APPTAINER_EXTRA_PATH` variable. 

The YAML configuration files for GitLab (static and dynamic) and Github, containing a job running with Apptainer will then look like:

:::{admonition} `.gitlab-ci.yml`
``` yaml
GEN:<proj_name>:
    extends: .generate_project
    variables:
      extends: .vars
      PROJECT_NAME: <proj_name>
      HOG_APPTAINER_IMAGE: <path/to/sif/file>
      HOG_APPTAINER_EXTRA_PATH: <external/path/for/apptainer>
```
:::

:::{admonition} `ci.conf`
``` ini
[generate_project]

[generate_project:variables]
HOG_APPTAINER_IMAGE=<path/to/sif/file>
HOG_APPTAINER_EXTRA_PATH=<external/path/for/apptainer>
```
:::

:::{admonition} `.github/workflows/hog-pull.yml`
``` yaml
name: Deploy

on:
  pull_request:
    branches: [ <Repository/Main/Branch> ]

jobs:
  hog-workflow:
    uses: hog-CERN/Hog/.github/workflows/Hog-pull.yml@Hog2023.1
    secrets:
      SUBMODULE_CONTENT_PULL_KEY: ${{ secrets.SUBMODULE_CONTENT_PULL_KEY }}
      HOG_PUSH_TOKEN: ${{ secrets.HOG_PUSH_TOKEN }}
      HOG_EMAIL: ${{ secrets.HOG_EMAIL}}
      HOG_USER: ${{ secrets.HOG_USER}}
      HOG_PATH: ${{ secrets.HOG_PATH }}
      HOG_XIL_LICENSE: ${{ secrets.HOG_XIL_LICENSE }}
      HOG_APPTAINER_IMAGE: <path/to/sif/file>
      HOG_APPTAINER_EXTRA_PATH: <external/path/for/apptainer>
    with:
      BUILD_PROJECTS: >-
        ["<proj_name>"]
      SIM_PROJECTS: >-
        ["<proj_name>"]
```
:::

### Hog-CI with Docker

This feature works only with GitLab CI/CD at the moment. To operate, the GitLab CI/CD with a docker image, you should set the `image:` parameter in your `.gitlab-ci.yml`, for the jobs that you would like to run with Docker. 

Please note, that this jobs will still run on your private runner machines, and not on shared-runners, which do not have enough memory to run the resource-demanding FPGA workflows. 

The image to use must be publicly accessible. CERN users can use the docker images already produced by the CCE group, which are accessible [here](https://gitlab.cern.ch/cce/docker_build/container_registry/). For example, to setup a job with Vivado 2019.1, the `.gitlab-ci.yml` job section should contain:

```yaml
your_job:
  image: gitlab-registry.cern.ch/cce/docker_build/vivado:2019.1
```

We suggest to pull the images you need before starting the CI, by logging into your machines and issuing the following commands:

```shell
docker login gitlab-registry.cern.ch # If you use images stored at CERN
docker pull <my_image>
```




