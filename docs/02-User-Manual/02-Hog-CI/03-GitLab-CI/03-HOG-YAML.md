## Configure your YAML file
In this paragraph, we describe the jobs of the Hog CI pipelines that need to be configured in your local `.gitlab-ci.yml` file. Only the jobs that are *project-specific* must be defined in this file. General jobs are directly included from the `hog.yml` reference file and are not configurable.

The configurable jobs are:

- Generation
- Simulation

### Generation Job

From Hog2020.2, all project related jobs are merged in a single *Generation* job, in order to minimise calls to git and to Vivado/Quartus.

The Generation job calls the `generate_project` function, creates and builds the Quartus/Vivado project, using the `Hog/Do WORKFLOW` script.

It can be configured with the following variables:

- `PROJECT_NAME` : (**Mandatory**) name of the Hog project you want to create
- `HOG_CHECK_SYNTAX` : (**Optional**) if 1, it checks the syntax of the created project. Better if defined globally as an environmental variable in your [Gitlab repository](01-setup-CI.md)
- `HOG_NJOBS`: (**Optional**) number of jobs to run the synthesis. It shall be defined in your [Gitlab CI/CD variables](01-setup-CI.md). Default: 4
- `HOG_ONLY_SYNTH` : (**Optional**) If set to 1, Hog-CI will run only the synthesis for the project. Default: 0
- `HOG_NO_BITSTREAM`: (**Optional**) If set to 1, the Hog-CI will not write a binary file for your project. Default: 0
- `HOG_IP_PATH`: (**Optional**) path to the folder where the IP generated results are stored. If defined, the job will copy the IP products from it without relaunching the IP synthesis/implementation to speed up the pipeline. If the stored IP products are outdated, the script will regenerate and copy them into the folder. It shall be defined in your [Gitlab CI/CD variables](01-setup-CI.md). If the path starts with `/eos/`, Hog will assume that the folder is on the EOS cloud space.

The resulting job in your `.gitlab-ci.yml` file, for the project `my_project` is,

```YAML
generate_project:my_project:
    extends: .generate_project
    variables:
      extends: .vars
      PROJECT_NAME: my_project
      HOG_ONLY_SYNTH : 0
      HOG_NO_BITSTREAM : 0
      HOG_CHECK_SYNTAX : 0
```

### Simulation Job (Vivado only)
The Simulation job calls the `simulate_project` function and launches a behavioural simulation for each `.sim` list file in your Hog project `list` directory. By default, simulation is executed using *XSim*. If you wish to use another simulation software, add the following line to the top of your `.sim` list file:

```
#Simulator xsim # For Vivado Simulator
#Simulator questa # For Questa Advanced Simulator
#Simulator modelsim # For ModelSim Simulator
#Simulator riviera # For Riviera-PRO Simulator
#Simulator ies # For Incisive Enterprise Simulator (IES)
#Simulator xcelium # For Xcelium Parallel Simulator
#Simulator vcs # For Verilog Compiler Simulator (VCS)
```

If you wish to skip the simulation of a particular simset, add at the top of the `sim` file the following keyword:

```
#skip_simulation
```

`simulate_project` requires two variables:

- `PROJECT_NAME` : (**Mandatory**) name of the Hog project you want to simulate
- `HOG_SIMULATION_LIB_PATH`: (**Mandatory for all simulators except xsim**) Path to the compiled simulation library in your VM. It shall be defined in your [Gitlab CI/CD variables](01-setup-CI.md).

The resulting job in your `.gitlab-ci.yml` file, for the project `my_project` is,

```YAML
simulate_project:my_project:
    extends: .simulate_project
    variables:
      extends: .vars
      PROJECT_NAME: my_project
```

```{note}
From Hog-2020.2, the *Simulation* job will run in parallel with the *Generation* job, since it does not depend of it. 
```
