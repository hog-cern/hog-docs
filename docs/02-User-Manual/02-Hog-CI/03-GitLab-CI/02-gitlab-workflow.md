## GitLab Work-Flow

To fully exploit GitLab features, the work-flow should start with the creation of a new issue and a related merge request and branch.

To do this, go to the GitLab website and navigate to your repository.

Click on issues and open a new issue describing the fix you are to implement or the new feature you want to introduce.
Once you have an issue, you can open a merge request marked as *Draft* (i.e. work in progress) and a new branch simply by clicking `Create merge request` inside the issue overview.

```{note}
From GitLab 13, Work In Progress (WIP) merge requests have been renamed *Draft*. More info [here](https://docs.gitlab.com/ee/user/project/merge_requests/work_in_progress_merge_requests.html)
```

It is possible to customise the branch name to tell Hog to increase the minor or major number. By default, this is done by having the branch name starting with `minor/` or `major/`. These keywords can be configured in the `Top/repo.conf` file. For more information have a look at the relative [chapter](../02-Hog-Versioning).

```{warning}
This automatic versioning scheme is available only from Hog2023.1. In older versions, when creating the merge request, you can write the *MINOR_VERSION* or *MAJOR_VERSION* keywords in the merge request description, to tell Hog how to tag the repository once the source branch is merged into your official branch.
```

Now that you have a new branch connected to the issue, on your local machine, navigate to your local project folder and checkout the new branch.

You can now develop your new feature or commit the code you have.

Once you are done with your changes, you are ready to start the Hog CI.

To do this, you need to solve the `Draft` status. This can be done either by going to the Merge Request page in the Gitlab web-site and click on the `Mark as ready` button,

<img style="float: middle;" width="700" src="../figures/resolve-wip.png">

or (thanks to a Hog feature) you can start your commit message with `RESOLVE_WIP:` from the command line.

```bash
  git add new_files
  git commit -m "RESOLVE_WIP: <my message>"
  git push
```

If you opt for the website method, remember that you need to push another commit afterwards to start the CI. Once the pipeline passes and your changes are approved, if required, you can merge your source branch simply by clicking on the merge button in the merge request.

<img style="float: middle;" width="700" src="../figures/merge.png">

### Create a Merge Request without an issue

You can avoid using issues by creating a new branch and a merge request connected to your branch.
You can still use the nice `Draft` feature by adding `[Draft]` or `Draft:` at the beginning of the title of the merge request: the merge request will be [marked as work in progress](https://docs.gitlab.com/ee/user/project/merge_requests/work_in_progress_merge_requests.html).

### Commit your code and accidental commits

If you have already some uncommitted/committed new features, you should create a new branch, commit your code there and create a new merge request when ready.

If you have already committed your changes to a wrong branch (e.g. the `master`) simply reset that branch to the latest correct commit, e.g.
```bash
  git reset --hard <latest_correct_commit_hash>
  git push origin master
```

Create a new branch, check it out and commit your code there. To avoid direct commit to the master (or official) branch, you can configure the repository to forbid pushing directly to the some special branch, for example master.

To do this go to Settings and then Repository on Gitlab website. Then expand Protected Branches and add the branches that you want to protect.

### Gitlab Releases
When creating a new tag, Hog CI will also create a new release, if the `__HOG_CREATE_OFFICIAL_RELEASE__` variable is set to 1.
The release contains the project version and resource utilization tables, and stores the binary files and logs, as derived from the _Collect Artifacts_ job.
In case of multiple projects, the release will always contain the latest available binaries. If a project was not generated in the current release (e.g. because it was not modified), the binaries attached to the release will be taken from the previous tag.   
Hog has also the ability to write a changelog automatically in the note, by looking at the merge request commit messages. If you want a commit message to be included in the release note, you should start your commit message with the `FEATURE:` keyword. For example:

```bash
  git commit -m "FEATURE: Some awesome update"
```

Finally, from Hog2022.1 the Merge Request description is also copied inside the GitLab release.

### Intermediate build branches

```{warning}
This feature is not available with the dynamic CI.
```

A novel feature of Hog2021.1 allows to execute a different CI build for Merge Request targeting an intermediate build branch. A possible use-case for this feature would be in the event that you have a large design, which you want to build only when the compilation of its individual module is successful.

To do that, you have to declare your intermediate branch in the global __HOG_INTERMEDIATE_BRANCH__ CI/CD variable. Then you have to select the projects that you wish to run for the intermediate branch MR, by setting to 1 the __INTERMEDIATE_BUILD__ variable in your `.gitlab-ci.yml` for the chosen project. For example,

```YAML
generate_project:my_project:
    extends: .generate_project
    variables:
      extends: .vars
      PROJECT_NAME: my_project
      INTERMEDIATE_BUILD: "1"

simulate_project:my_project:
    extends: .simulate_project
    variables:
      extends: .vars
      PROJECT_NAME: my_project
      INTERMEDIATE_BUILD: "1"
```

In this case, CI workflow for project `my_project` would start only for merge requests towards the __HOG_INTERMEDIATE_BRANCH__.

```{note}
Merging branches into __HOG_INTERMEDIATE_BRANCH__ do not activate any subsequent pipeline. You have to open manually another MR from __HOG_INTERMEDIATE_BRANCH__ to __HOG_TARGET_BRANCH__, if you wish to propagate your changes.  
```
