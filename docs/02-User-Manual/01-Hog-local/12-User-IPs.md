## User IPs

Xilinx Vivado handles the user generated IPs by collecting them into IP repository. Their location can be usually set inside the Vivado GUI. 

This shall be also set in the `hog.conf` file under the `[main]` section, by defining the `IP_REPO_PATHS` variable. 

```ini
#vivado
[main]
PART = xcku15p-ffva1760-2-e
IP_REPO_PATHS = "<my_path>"
```

It is also possible to have multiple user IP repository, simply writing multiple paths separated by spaces, e.g.

```ini
IP_REPO_PATHS = "path1/ path2/ path3/"
```

:::{warning}
Starting from version Hog2022.1, no default value will be set for the `IP_REPO_PATHS` variable. Therefore, if you update from an older version of Hog and use the old default `IP_repository` path, you need to set it in the project `hog.conf`.
:::
