## Hog and Sigasi
Hog supports the [Sigasi](https://www.sigasi.com/) suite.

A script able to generate a CSV file compatible with Sigasi is provided here: `Hog/Other/CreateSigasiCSV.sh`[^shell].
[^shell]: This is a shell wrapper for the Tcl script: `Hog/Tcl/utils/create_sigasi_csv.tcl`

:::{admonition} Support
:class: note

For the time being this only works with Xilinx Vivado. Quartus and ISE support will be provided in next Hog releases.
:::

This script can be used to create a CSV file containing all the project files (and the libraries) used to create Sigasi project.

It creates two files, one for simulation and one for synthesis called respectively: `<project name>_sigasi_sim.csv` and `<project name>_sigasi_synth.csv`.

The generated CSV files can be used to create a Sigasi project as explained in Sigasi website [here](https://insights.sigasi.com/tech/generating-sigasi-project-vivado-project/#2-generate-the-sigasi-project-files-from-the-csv-files).
