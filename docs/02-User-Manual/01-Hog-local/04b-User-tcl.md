## Custom User Tcl scripts in build flow


### User Tcl scripts at project-creation time
If you wish to run some scripts before of just after creating your project you can add a Tcl file called:

- `Top/<project name>/pre-creation.tcl`.
- `Top/<project name>/post-creation.tcl`.
- `Top/hog.tcl`.

The pre-creation script, can be used to generate additional files to be included in your project.

The post-creation script can be used to modify properties of files that are added to the project. 

The `hog.tcl` script is common to all your projects, and will be sourced before the creation of any project in the Top directory.

In these scripts, you can use all the commands available from the IDE, plus the functions defined in `Hog/Tcl/hog.tcl`.

:::{note}
The post-creation script can be used, for example, to integrate a pin assignment in your Quartus project. To do this export your pin assignment as a Tcl script form the Quartus Pin planner (File > Export) and source the resulting script after calling `./Hog/Tcl/create_project.tcl`.
:::

:::{note}
It is possible to add files to the project using these scripts, or even generate files and then add them. From Hog2022.1, you can generate any kind of files using `tcl` scripts. Hog will find out that file not listed in the list files were added and will save the md5sum of the generated files at creation time into a `.hog` folder, inside the `Projects` directory. At synthesis time, the md5sum will be compared against what it has been saved, and if different a warning message will be printed.
:::

### User Tcl scripts during the workflow
Hog can run automatically some scripts every time the compilation workflow is started or during any step in the workflow, they will be run immediately *after* the Hog scripts.

The user scripts shall be named as:

- `Top/<project name>/pre-simulation.tcl`: it will be run before any simulation set (BATCH MODE ONLY); 
- `Top/<project name>/pre-<simualation set>-simulation.tcl`: it will be run before the specific  simulation set (BATCH MODE ONLY);
- `Top/<project name>/pre-synthesis.tcl`: it will be run before project synthesis, after Hog pre-synthesis script;
- `Top/<project name>/post-synthesis.tcl`: it will be run before project synthesis, after Hog post-synthesis script;	
- `Top/<project name>/pre-implementation.tcl`: it will be run before project implementation (Vivado/ISE), after Hog pre-implementation script;
- `Top/<project name>/post-implementation.tcl`: it will be run after project implementation (Vivado/ISE) or project fit (Quartus), after Hog post-implementation script;
- `Top/<project name>/pre-bitstream.tcl`: it will be run before bitstream after Hog pre-bitstream script;
- `Top/<project name>/post-bitstream.tcl`: it will be run after bitstream, after Hog post-bitstream script.

If you are working with AMD Versal devices, you can also add `Top/<project_name>/pre-platform.tcl` script, which runs before 

:::{warning}
In Vivado, when you write these scripts, you cannot always use the same commands that you would use from the Tcl console in the project. Unfortunately when Vivado runs the synthesis and the implementation, it creates a sort of parallel project, so instructions like `get_run` or `get_files` might not give the expected results. The recommended way to find out is to try some test script with some printouts.
:::