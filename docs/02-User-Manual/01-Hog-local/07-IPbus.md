## Hog and IPbus
Hog supports [IPbus](http://ipbus.web.cern.ch/ipbus/) by handling IPbus XML files and VHDL address maps.

To use IPbus with Hog, include it as a submodule in your HDL repository. Include IPbus files in a [list file](02-List-files.md).

### Embedding of version and SHA in the XML files
Hog keeps track of the version and SHA of the XML files by means of two dedicated generics/parameters (XML_VER and XML_SHA) and dedicated node tags in the XMLs.

To allow for this, your project must include one or more list files with `.ipb` extension located in the project's `list` folder.

```shell
<repository>/Top/<project_name>/list/<file_name>.ipb
```
.

Note that the XML and VHDL files can be located anywhere in your project.

### List files with `.ipb` extension

This kind of files contain the list of the XML files used to generate the IPbus modules together with the generated VHDL address decode files, each line has the form:

```shell
 <path_to_xml>/<address_table>.xml [<path_to_vhdl>/<generated_file>.vhd]
```

During pre-synthesis, Hog will loop over the files contained tin this file to retrieve the SHA of the latest commit in which at least one of them was modified.
The path to the generated module, if specified, enables Hog to use an IPbus python script ([`gen_ipbus_addr_decode`](https://github.com/ipbus/ipbus-software/blob/master/uhal/tools/scripts/gen_ipbus_addr_decode)) to verify that the generated modules correspond to the XML files.
To use this feature, IPbus must be correctly installed, i.e.: the above mentioned script must be in the PATH and be able to access all the libraries it needs.


:::{note}
The `.ipb` list files are normal Hog [list files](02-List-files.md), with the exception that **the listed files are not added to the project**. For this reason properties like `nosim` or `noimpl` do not make sense and cannot be used.

To porperly use the VHDL address map files, you need to add them to a `.src` file.
:::

#### IPbus xml files
Hog can back annotate the included XMLs with the SHA evaluated as described above.
This can be used by software to correctly assess if the used XMLs correspond to the gateware loaded on the device.

You can achieve this by defining a dedicated register where to store the value of the [generic](03-parameters-generics.md): `XML_SHA` provided by Hog.

The node corresponding to this registers is expected to have the following structure:

```xml
    <node id="gitSHA" permission="r" address="0x-"  tags="xmlgitsha=GIT_SHA" description="XML git commit 7-digit SHA of top file">
```

During pre-synthesis, Hog will replace `__GIT_SHA__` with the SHA of the latest commit in which at least one of XMLs was modified.
Hog will also set the `XML_SHA` generic in your top level to correspond to the same SHA.
The user can now verify it is using the correct version of the XMLs by comparing the `gitSHA` register content with the `gitSHA` register tag.

The same procedure is done for the xml version.
In this case the node is expected to have the following structure:

```xml
<node id="Version" permission="r" address="0x-" tags="xmlversion=__VERSION__"  description="version of XML files">
    <node id="Patch" mask="0xffff" description="Patch Number"/>
    <node id="Minor_Version" mask="0xff0000" description="Minor Version Number"/>
    <node id="Major_Version" mask="0xff000000" description="Major Version Number"/>
</node>
```

The `__VERSION__` will be set to the version of the xml files taken from the last tag in which at least one of the xml files included in any of the `.ipb` list files was modified.
The same value will be reported in the `XML_VER` generic of the top level of your project.

### Check address maps against xml file
Hog can handle IPbus XML files automatically.
To run it from a console, use the following syntax:

```console
Hog/Do XML <project name> [-xml_dir <destination directory>] [-generate]
```

This script will copy all the IPbus XML files listed in the `.ipb` list files (open them recursively if necessary) of the specified project into the `<destination directory>` creating it if necessary.
If no `<destination directory>` is provided via the `-xml_dir` option, Hog will use `../<project_name>_xml` by default.

Hog will aslo perform the substitution of the `__GIT_SHA__` and `__VERSION__` placeholders as described above.
This is useful as IPbus XML files need to be all in the same directory to work. You can use this to distribute your XML files together with your binary file.

If the `gen_ipbus_addr_decode` IPbus python script file is in your PATH and works correctly, Hog will also verify each xml file against its VHDL address map to see if they match. It will do this ignoring blank lines and comments and will give warnings if it find some mismatch.

### Generation of VHDL address maps
If the `-generate` options is used, Hog will use the `gen_ipbus_addr_decode` to generate the VHDL address map files and replace them if necessary.
