## Project configuration file

Hog reads all the project properties from a configuration file called `./Top/<my_project>/hog.conf` to generate the HDL project.

The `hog.conf` file is expected to contain at least few basic variables with the information needed to build your project.
This section contains a full recipe to write a `hog.conf` file for your project.

Templates for a Vivado, PlanAhead and Quartus project can be found under `./Hog/Templates/hog_*.conf`.

This is an example of the minimal `hog.conf` file needed for a Vivado project, specifying only the device:
```ini
#vivado 2020.2
[main]
PART = xc7a35tcpg236-1
```

### Telling Hog the IDE tool to be used

The first line of your configuration file is expected to be a comment indicating which IDE tool and what version must be used to generate your project.
The following tools are recognised:

- vivado
- planahead
- quartus
- libero

After the name of the tool, the version must be specified as in the following examples:
```ini
#vivado 2020.2
#vivado 2020.1_AR75210
#quartus 19.1.0
#planahead 14.7
#libero 2022.2.0.10
```

The version must be in the format `<a>.<b>[.<c>]`, i.e. it can be 2 or 3 numbers separated by dots. There must be at least a space between the IDE name and the version, while the spaces between the '#' and the compiler name are optional, so the following syntaxes are also acceptable:

```ini
#vivado 2019.2.1
# vivado 2019.2
#  vivado   2019.2
```

If this line is not available Hog will assume your project is a Vivado project, but will give you a critical warning.

#### Why do wee need the tool version?
Specifing the version is important to ensure that the project is run with the required tool version.

Project properties specified in the `hog.conf` file might be incompatible among different IDE versions.
More importantly the Intellectual Properties (IPs) are linked to a specific version and are not inter-version compatible.

If you use a tool that is older than the one specified in `hog.conf` Hog will simply refuse to create the project and throw an Error. In this case, if you really want to create the project, you can modify the version in `hog.conf` and retry. The probability of success are tiny though.

If you use a newer tool than the one specified, Hog will create the project but will throw a critical warning, reminding you that if you mean to upgrade your project to work with a newer tool version, you also need to update the version number in `hog.conf`. If the project contains IPs you will have to update those too and possibly modify some project properties in `hog.conf`.

:::{warning} PlanAhead and Quartus are currently partially supported.
:::

### Project description
The second line of `hog.conf` can be used to provide a description of the project.
If the second line of `hog.conf` is a comment, this is used as a description for the project when `./Hog/Do LIST` is called.
The line is stripped of all initial `#` and spaces and reported after a dash in the list, as shown here:

```
** The projects in this repository are:

Group1/Project1 (vivado 2023.2) - The description is displayed here
Group1/Project2 (vivado 2023.2) - Another description

group2/Project1 (vivado 2020.2) - Yet another description
group2/Project2 (vivado 2023.1)
```

If the description contains the word test, Test or TEST, then the project is not shown in the list unless the option `-all` is specified.
In this case, previously hidden project will show "Test project" as a description.


### Main section: project variables

The configuration file needs to contain a `main` section specifying at least the following variables:

#### PART
The `PART` variable indicates the target device code for your project.
This variable is *mandatory*.
It must be chosen among the ones provided by the chosen HDL compiler.
As an example for a Xilinx Virtex-7 FPGA it could be set to `xc7vx330tffg1157-2`.
Note that the exact code will depend on the full characteristics of the device you are using, e.g. number of logic cells, package, speed grade, etc.

#### FAMILY
The `FAMILY` variable indicates the device family.
This variable is mandatory for *Quartus only*.
The value must be chosen among the ones provided by the chosen HDL compiler.
As an example for a Intel MAX10 FPGA it must be set to "MAX 10".
Please note that the variable value is included in quotation marks.

### Synthesis and implementation sections (Xilinx only)
In Vivado you can set properties at 3 levels: project, synthesis, implementation.
To do this you need to create the specific section of your hog.conf file: `main`, `synth_1`, `impl_1`.

This is an example of a configuration file for a Vivado project that includes several properties:

```ini
#vivado 2020.2
#This is the description of this project
[main]
PART = xc7vx550tffg1927-2

[synth_1]
STEPS.SYNTH_DESIGN.ARGS.RETIMING = true

[impl_1]
STEPS.WRITE_BITSTREAM.ARGS.BIN_FILE = 1
STEPS.OPT_DESIGN.ARGS.DIRECTIVE = Default
```

To find out the exact name and value of the property, use Vivado GUI to click on the checkbox you like.

![](./figures/tick_gui.png)

This will make Vivado run the set_property command in the Tcl console.

![](./figures/Vivado_tcl.png)

Then copy and paste the name and the values from the Vivado Tcl console into the lines below.

:::{admonition} Vivado Strategies
Vivado strategies are in reality a set of directives that changes different parameters. Unfortunately, Hog is not able to compare the properties set by the strategies against the one specified in the `hog.conf` file. For this reason, if you specify the `STRATEGY` inside the `hog.conf`, you will get a Warning, notifying that the config check is disabled.

To avoid this, we suggest to set the Strategy in Vivado and then regenerate the `hog.conf` file using the provided button. The new file will then contain all the properties of the chosen strategy, without the `STRATEGY` parameter itself.
:::

### Parameters section (Xilinx only)
Vivado projects contain volatile parameters that must be set before launching each run: synthesis and implementation.

The `parameters` optional section is used for this purpose. The most important volatile parameter is MAX_THREADS that specifies how many cores to use in synthesis and implementation.

By default Hog sets this value to 1 to assure that the binary files production is deterministic.

This is an example of the parameters section:

```ini
[parameters]
MAX_THREADS = 16
```

### Hog section
The optional `Hog` section is used to specify Hog directives valid for the project.

The supported properties are:

- `ALLOW_FAIL_ON_CONF`: will not set the version to 0 in HDL registers, even if the conf file is modified with respect to the project (The Critical Warning is still raised)
- `ALLOW_FAIL_ON_LIST`: will not set the version to 0 in HDL registers, even if any of the list files is modified with respect to the project (The Critical Warning is still raised)
- `ALLOW_FAIL_ON_GIT`: will not set the version to 0 in HDL registers, even if a file is found in the project which is not committed to the repository (The Critical Warning is still raised)
- `EXPORT_XSA`: will either force or prevent the export of an XSA file for Xilinx Zynq devices (documentation [below](#xsa-export-for-zynq-devices-xilinx-only))
- `PASS_GENERICS_TO_BD_IPS`: will pass generics to block design IPs. (documentation [below](#passing-generics-to-bd-ips))

### Generics section
From Hog2023.1, it is possible to pass custom generics to the top module of your project. These can be defined in the `[generics]` section of the `hog.conf` file. Of course, generics (or parameters in Verilog) must be defined accordingly in the source code. An example generics section looks like:

```ini
[generics]
MY_GENERIC_INT=0
MY_GENERIC_STRING="a string"
MY_GENERIC_HEX=4'h7
```

At the moment, this functionality is supported only by Xilinx and Microchip IDEs. 

### XSA Export for Zynq Devices (Xilinx Only)
For Xilinx Zynq devices, integration of the gateware with the Linux system uses a Xilinx Support Archive (XSA) file.

An XSA file is a zip archive containing files such as Vivado tool versions, part numbers, the compiled bitstream, C headers, and the Hardware Handoff File (HHF) that contains information describing the Processor-to-FPGA-interface implemented in the Vivado Block Designer.

Hog supports the automatic generation of XSA files for Zynq devices.

In the post-bitstream stage of compiling Zynq gateware, Hog will check the FPGA part number. If it is determined to be a Zynq device, Hog will automatically run the `write_hw_platform` command which is used to export an XSA file from a Vivado project.
Detection of a Zynq part is using a simple regex for parts beginning with `xc7z` or `xczu`; this will need to be updated in the future as new parts are released by Xilinx.

If automatic detection of a supported device fails, generation of an XSA file can be forced with the `EXPORT_XSA` property in `hog.conf`.
Conversely, if export of XSA files is not desired, it can be disabled with the same property.

``` ini
[hog]
# set true to force creation of an XSA
EXPORT_XSA=true
# set false to stop creation of an XSA
EXPORT_XSA=false
```


### Passing Generics to BD IPs
Vivado currently only offers one way to natively pass generics to a block design's internal IP, which is to generate the block design and its IPs in "Out-of-Context" mode (See [Out-of-Context Synthesis](https://docs.amd.com/r/en-US/ug949-vivado-design-methodology/Out-of-Context-Synthesis)). In this mode, Vivado passes the top level project's generics to each IP during synthesis, which includes all Hog-related generics. However, if the user does *not* want to generate the block design using out-of-context mode, there is no native way to pass generics to the IPs.

To work around this, you can tell Hog to manually apply the generics to the block designs' IPs. This is done by setting the `PASS_GENERICS_TO_BD_IPS` property in the `hog.conf` file. This property can be set to `true` to pass generics to all block design IPs, or to a regex string to pass generics to block design IPs with names that match the regex. 

``` ini
[hog]
# set true to pass generics to all block design IPs
PASS_GENERICS_TO_BD_IPS=true

# set to regex to pass generics to BD IPs with names that match the regex: 
PASS_GENERICS_TO_BD_IPS=".*ip_name.*"

# example passing generics to two IPs:
PASS_GENERICS_TO_BD_IPS=".*ip_name1.*|.*ip_name2.*"
```

Hog will check each IP in the block design for hog-related generics and only apply them if they are found (See [hog generic](03-parameters-generics.md#parameters-generics) and [custom generics](#generics-section) for the list of supported generics that hog will attempt to apply to the IPs).

:::{warning}
When using this feature, there is a chance that the block design's `.bd` file will be updated to include the last used generics. For example, notice how this IP's parameters were updated to include the generic:
``` 
+     "parameters": {
+          "MY_GENERIC_INT": {
+            "value": "0"
+          }
+     },
```

Hog will always overwrite these values during both project creation and anytime a new synth run is started to ensure they are up to date and match whatever is in the `hog.conf` file.

This feature is experimental and may not work as expected in all cases. If you encounter issues, please report them to the Hog developers.
:::
