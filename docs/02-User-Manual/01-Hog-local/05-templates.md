## Templates

Hog provides templates for the most important files needed in your HDL repository.
The templates are located in the [Hog/Templates](https://gitlab.com/hog-cern/hog/-/tree/master/Templates) directory.
Here is a list of the available templates:

### hog.conf
`hog_vivado.conf`, `hog_ise.conf`, `hog_minimal` and `hog_quartus.conf` are examples of configuration files.
They contain examples of some of the the variables used by Hog to generate a project

To use one of these files, copy it to the `Top/<project>/` directory, rename it to `hog.conf`  and modify it to accommodate your needs as explained [here](01-conf).

### top.vhd
`top.vhd` is an example of top level file in VHDL. It contains the definition of the generics set by Hog to keep track of the HDL design versions, as explained [here](03-parameters-generics).

To use this file, copy it anywhere in your project, rename it and modify it to accommodate your project. Remember that the contained entity must be called `top_<project_name>` and that this file name and path must be included in a `.src` list file in `Top/project/list/` directory.
Alternatively, you can use a different name for your top entity and specify it with the "top" property in the `.src` list file.

### top.v
`top.v` is an example of top level file in Verilog. See `top.vhd` for details.

### github-pull.yml
`github-pull.yml` is an example of YAML configuration file to setup the Hog-CI in GitHub Actions, for each commit in a pull-request branch.

To use this file, copy it in the `.github/workflows/` folder of your repository. Modify the file replacing the place holder with the name of your project(s). 

### github-main.yml
`github-main.yml` is an example of YAML configuration file to setup the Hog-CI in GitHub Actions, for each commit to the main branch of the repository.

To use this file, copy it in the `.github/workflows/` folder of your repository. 

### github-tag.yml
`github-tag.yml` is an example of YAML configuration file to setup the Hog-CI in GitHub Actions, for each new created tag. 

To use this file, copy it in the `.github/workflows/` folder of your repository, and modify the GitHub inputs accordingly.


### gitlab-ci.yml
`gitlab-ci.yml` is an example of YAML configuration file for the GitLab CI. It contains definitions of the jobs to be run for a project in your repository. To use this file, copy it to the root folder of your repository and rename it to `.gitlab-ci.yml`. Modify the file replacing the place holder with the name of your project. If you have several projects in your repository, you should copy and paste the file content several times and change the place holder with the names of all your projects.

### gitlab-ci_dynamic.yml
`gitlab-ci_dynamic.yml` is an example of YAML configuration file to use the novel Hog dynamic CI in your project. To use this file, copy it to the root folder of your repository and rename it to `.gitlab-ci.yml`. Modify the file, replacing the `ref:` line with the chosen version of Hog (N.B. the dynamic CI is available from Hog-2020.2)

### gitlab-ci_parallel.yml
`gitlab-ci_parallel.yml` is an example of YAML configuration file for the GitLab CI. It exploits the novel GitLab [`parallel:matrix` feature](https://docs.gitlab.com/ee/ci/yaml/#parallelmatrix), to run multiple times the same script with different variables (e.g. different `PROJECT_NAME`). 

To use this file, copy it to the root folder of your repository and rename it to `.gitlab-ci.yml`. Modify the file replacing the place holder with the name of your projects. 

### ci.conf
`ci.conf` is the an example configuration file for a project in your repository that has to be run by the Hog dynamic CI. Copy it in the `Top/proj` folder and write in the file the stages that you want to run. For more information refer to the [Hog Dynamic CI](../02-Hog-CI/03-GitLab-CI/05-Dynamic-CI) section.

### gitignore
`gitignore` is an example of `.gitignore` file. This file tells git which files should be ignored. Files covered by a rule in this file will not be uploaded to your repository.
The template contains rules for all the files used by the supported HDL compilers that should not be tracked by git. To use this file copy it to the root folder of your repository and rename it `.gitignore`.
This file has special rules for the `IP` and `BD` folders. In the `IP` folder only the .xci files[^xci files] contained in a subfolder should be considered, while in the `BD` folder only the .bd files. This is done because the GitLab CI needs to export the so-called artifacts so Hog has to know *a priori* where all the additional files will be created by the HDL synthesiser.

The provided template only works for a simple directory structure, but more complex structures can be used in Hog, as long as **all the `.xci` files** are contained in the `IP` folder and **all the `.bd`** files are contained in the `BD` folder, you can use as many subdirectories as you need. In this case you will have to write your own `.gitignore` file, remember that it is possible to use multiple `.gitignore` files placed in sub directory in your repository.

[^xci files]: You will need to modify this file to accommodate Quartus projects.

### doxygen.conf
`doxygen.conf` is an example of Doxygen configuration file optimised for VHDL. To use this file, copy it to a folder named `doxygen` in the root folder of your repository, modify to accommodate you project. You can add more files to the `doxygen` directory such as figures, .css files, .html, .css files for your documentation.

### repo.conf
This file needs to be located in the `Top` folder of your local clone of
the repository. The values used in this template are the default ones but
feel free to edit them to accommodate your needs.

### sim.conf
This file is an example configuration for the simulation properties of a Vivado project. Copy it in your `Top/<my_vivado_project>/` folder, and edit it to accommodate your needs.

