## List of supported tools

Below you can find a list of supported tools.

```{warning}
The Hog team does NOT provide access to the listed tools and it does NOT grants any help on the usage of the tools.
```

The list is intended as a list of tools you can use in an Hog compatible project and be sure The Hog style CI will be able to run on your project.
The following list is not exclusive, meaning other tools might be used in your project and still work with the Hog CI scripts.

- Vivado (earliest tested: 2017.1 latest tested: 2024.2)
- Quartus (earliest tested: 19.1 latest tested: 19.2)
- QuestaSim[^questa] (earliest tested: 10.7a latest tested: 2019.2)
- ISE/planAhead (tested only with version 14.7)
- Aldec Riviera Pro (tested with version 2020.10)
- doxygen (tested only with version 1.8.17)
- git (earliest tested: 2.7.0 latest tested: 2.38.1)
- MicroChip Libero SoC (latest tested: 2024.2)
- Lattice Diamond (latest tested 3.14)

[^questa]: Depends on Vivado version used. Check the compatibility list at: <https://www.xilinx.com/support/answers/68324.html>
