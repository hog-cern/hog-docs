## Using Hog with Libero SoC

From `Hog2025.1`, we added support to [Lattice Diamond](https://www.latticesemi.com/latticediamond). To use Hog with Diamond, some special configurations must be set by the users.

### Tcllib

Unfortunately, Diamond does not ship with a full version of `tcllib`. Therefore, Hog has to use the system installed `tcllib` library. 

This can be installed easily,

```shell
sudo apt install tcllib # On Ubuntu
sudo yum install tcllib # On Fedora/CentOs/RedHat
sudo zypper install tcllib # On OpenSuse
```

Once installed, you must tell Hog the location of the `tcllib` libraries, by setting the `HOG_TCLLIB_PATH` accordingly. E.g.

```shell
export HOG_TCLLIB_PATH=/usr/share/tcl/tcllib1.20/
```

This variable must be also set in the CI, following the instructions [for GitLab CI/CD](../02-Hog-CI/03-GitLab-CI/01-setup-CI.md#environment-variables) and [for GitHub Actions](../02-Hog-CI/04-GitHub.md#secrets-and-inputs). 

### Lattice Diamond's IPs
Diamond's IP are packed in the `.ipx` format. Simply include them in your `.src` list file, to use them in your project

```
# Example Diamond.src list file including a pll IP
diamond/ips/my_pll/my_pll.ipx 
```

<!-- ### CI requirements
In addition to the `tcllib`, to execute Hog with Libero on the Continuous Integration, you must also install the following package on your CI machine(s):

```shell
sudo apt install xvfb # On Ubuntu
sudo yum install xorg-x11-server-Xvfb # On CentOs, Fedora, RedHat
sudo zypper install xorg-x11-server-extra # On OpenSuse
``` -->

### Simulations
Currently, we do not support any simulation tools for Libero SoC. Support is planned in future releases. 



