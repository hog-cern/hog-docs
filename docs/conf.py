# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))

# -- Project information -----------------------------------------------------

project = 'Hog'
copyright = '2018-2025 The University of Birmingham'
author = 'N. Aranzabal, N. Biesuz, D. Cieri, F. Gonnella, N. Giangiacomi, G. Loustau De Linares, A. Peck'

# The full version, including alpha/beta/rc tags
#release = ''

# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    "myst_parser", "sphinx_copybutton", "sphinx_togglebutton",
    "sphinx.ext.autodoc", "sphinx.ext.intersphinx", "sphinx.ext.viewcode",
    'notfound.extension', "versionwarning.extension", "sphinx_design"
]
#"sphinx_tabs.tabs",

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []

# source_suffix = {'.rst': 'restructuredtext'}

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
# html_theme = 'alabaster'

html_theme = "sphinx_book_theme"

# html_logo = "_static/logo.png"
html_title = ""
html_theme_options = {
    "path_to_docs": "docs",
    "repository_url": "https://gitlab.com/hog-cern/Hog",
    # "repository_branch": "gh-pages",  # For testing
    # "use_edit_page_button": True,
    "use_issues_button": True,
    "use_repository_button": True,
    "use_download_button": True
    # "expand_sections": ["reference/index"]
    # For testing
    # "home_page_in_toc": True,
    # "single_page": True
    # "extra_footer": "<a href='https://google.com'>Test</a>",
    # "extra_navbar": "<a href='https://google.com'>Test</a>",
}

# html_title = "My amazing documentation"
html_logo = "img/doxygen_logos_hog.png"
html_favicon = "custom/assets/images/hog.png"

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".

myst_enable_extensions = [
    "dollarmath",
    "amsmath",
    "deflist",
    "html_image",
    "colon_fence",
    "smartquotes",
    "replacements",
    "substitution",
]
myst_url_schemes = ("http", "https", "mailto")
myst_heading_anchors = 5
panels_add_boostrap_css = False

suppress_warnings = ["myst.header", "myst.lexing"]
