# Bibliography

- N.V. Biesuz, R. Caballero, D. Cieri, N. Giangiacomi, F. Gonnella, G. Loustau De Linares, A.Peck, *Hog 2023.1: a collaborative management tool to handle Git-based HDL repository*, Proceeding for the 3rd Workshop on Open-Source Design Automation (OSDA), 2023, [arXiv:2304.02437](https://arxiv.org/abs/2304.02437)

- N.V. Biesuz, D. Cieri, F. Gonnella, G. Loustau De Linares, A. Peck
, *Hog (HDL on Git): An easy system to handle HDL on a git-based repository*, Proceeding for the 15th Pisa Meeting on Advanced Detectors, [DOI: 10.1016/j.nima.2023.168016](https://doi.org/10.1016/j.nima.2023.168016)

- N.V. Biesuz, D. Cieri, N. Giangiacomi, F. Gonnella, A. Peck, *Hog: handling HDL repository on git*, Proceeding for TWEPP2021, [DOI: 10.1088/1748-0221/17/04/C04020](https://iopscience.iop.org/article/10.1088/1748-0221/17/04/C04020)

- F. Gonnella, *Hog (HDL on git): a collaborative HDL management tool*, Proceeding for TIPP2021, [DOI: 10.1088/1742-6596/2374/1/012100](https://iopscience.iop.org/article/10.1088/1742-6596/2374/1/012100)

- N.V. Biesuz, A. Camplani, D. Cieri, N. Giangiacomi, F. Gonnella, A. Peck, *Hog (HDL on git): a collaborative management tool to handle git-based HDL repository*, Techical report published in JINST 16 (2021) 04, T04006, [DOI: 10.1088/1748-0221/16/04/T04006](https://iopscience.iop.org/article/10.1088/1748-0221/16/04/T04006)

- F. Gonnella, *A collaborative HDL management tool for ATLAS L1Calo upgrades*, Proceeding for TWEPP2018, [DOI: 10.22323/1.343.0142](https://pos.sissa.it/343/142)