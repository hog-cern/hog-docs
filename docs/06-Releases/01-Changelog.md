# Changelogs

## Hog2025.1

- Support to Lattice Diamond.
- Pre-synthesis check for files in project and list files added to Libero and Diamond.
- New support for pre-simulation scripts for all simsets or specific simset.
- Added support to multiple IPbus list file.
- IPBus list file extension renamed `.ipb`.
- Added support to custom user command to be added to the `Hog/Do`.
- Included list files can be added with a specific reference path.
- Hog passes now parameters and generics to IPs within a Vivado block design.
- Option to save DCP checkpoints in Vivado synthesis and implementation, for incremental workflows.
- Updated GitLab badge to new format.
- Support to constraing scoping in Vivado.
- Pulse Width Timing included in timing analysis in Vivado.
- Support to hidden test projects.
- Option to add a project description in the `hog.conf` file.
- Support to VHDL2019 for compatible Vivado versions.

## Hog2024.2 

- Improved support for Hog-CI running on GitHub Actions.
- Renamed of `merge_and_tag` stage into `check_branch_state` in the Hog-CI.
- Hog-CI now makes use of the GitLab and GitHub CLI software, to perform all repository-related actions.
- Improved support for AMD Versal device
- For Versal, added a new pre-platform user-defined script that is executed ust before the generation of the XSA file.
- Changed default simulator software to Vivado Simulator (Vivado only).
- Improved support for MicroChip Libero SoC.
- Added a new parameter `HOG_SIMPASS_STR` into `sim.conf`. This allows users to specify a special keyword that, when found in the simulation log, will indicate that the simulation has passed.

## Hog2024.1 

- Added support to Xilinx IP Core Container `.xcix` files.
- Introducing common user `Top/hog.tcl` script, sourced by all projects at pre-creation time. 
- Coloured outputs for better logging, and summary files for warning and errors
- Improved support to Microsemi Libero 
  - `NDC`, `FDC` and `IO_PDC` constraint files are now supported
  - Reports in the output `bin` folder
  - Full support to project subgroups


## Hog2023.2 

- Reduced the amount of bash code in favour of tcl, introducing a single `tcl` launcher `launch.tcl`, capable of calling the correct IDE. 
- New `Hog/Do` command, which integrates all the functionalities of the old `CreateProject.sh`, `LaunchWorkflow.sh` and `LaunchSimulation.sh` scripts. More info in the [Usage Chapter](../../02-User-Manual/01-Hog-local/04-Usage).
- Improved support to simulations with Xilinx Vivado, with support of standard `sim_1` simulation file set, and possibility to use the same simulation library in multiple simsets. For more details, see the [Simulation Chapter](../../02-User-Manual/01-Hog-local/02b-Simulation).
- New `HOG_OFFICIAL_BIN_PATH` variable, replacing `HOG_OFFICIAL_BIN_EOS_PATH`. The new variable allows you to store the official binaries into a chosen folder inside your build machine. More info [here (GitLab-CI)](../02-User-Manual/02-Hog-CI/03-GitLab-CI/01-setup-CI) , and [here (GitHub Actions)](../02-User-Manual/02-Hog-CI/04-GitHub.md).
- Improved support to Versal devices.
- Improved look of Hog GitLab release, with collapsible sections for each built project.

## Hog2023.1 

- Reduced Hog printouts and included -verbose option
- Define and pass user generics to top module (Xilinx and Libero) and top simulation files. Please refer to the generics sections of the [project](../02-User-Manual/01-Hog-local/01-conf.md#generics-section) and [simulation](../02-User-Manual/01-Hog-local/02b-Simulation.md#generics-section) configuration file chapters, for more details.
- Added support to Microchip Libero SoC. More details on how to use Hog with Libero are described in the [dedicated chapter](../02-User-Manual/01-Hog-local/13-Libero.md).
- Added option to run the Hog-CI with Apptainer or Docker images. For the instructions, have a look at the [dedicated chapter](../02-User-Manual/02-Hog-CI/07a-Containers.md) in the CI section.
- Support to multiple release branches and new automatic tagging scheme. More informations [here](../02-User-Manual/02-Hog-CI/02-Hog-Versioning.md).
- The IP handling system (copying synthesised IPs to/from EOS or a local directory) was improved. The description of the Hog IP handling is available in the [dedicated chapter](../02-User-Manual/01-Hog-local/11-IPs.md).
- Support to GitHub actions to run the Hog-CI. The documentation on how to setup GitHub to run the Hog-CI are available in the [specific chapter](../02-User-Manual/02-Hog-CI/04-GitHub.md). 

## Hog2022.2 

- A new CI job is added to the `collect` stage, checking the timing closure of the built projects, and in case of a failed timing, it fails (with allow to failed option), signalling the users the problematic project.
- Simulation properties are now set using the new `sim.conf` file. Please refer to the [Simulation Chapter](../02-User-Manual/01-Hog-local/02b-Simulation.md) for the detailed instructions.
- A Hog version checker is launched every time the official shell scripts are sourced (`Init.sh`, `CreateProject.sh`, `LaunchWorkflow.sh` and `LaunchSimulation.sh`), checking the running version of Hog, and if a newer stable version is available, suggests the users to update.
- A hierarchical resource estimate is included in the artifacts and in the `bin` folder.
- IDE versions must be now specified in the `hog.conf` files.
- Adding option to create GitLab badges for selected projects, showing the resource utilisation, project version and timing closure status. 
- Restyled the dynamic-CI, with the option to add user jobs as in the standard CI. For more details, please have a look at the [Dynamic-CI chapter](../02-User-Manual/02-Hog-CI/03-GitLab-CI/05-Dynamic-CI.md)


## Hog2022.1 

- IP generated outputs can be saved by the CI on the running machine to speed-up the workflow.
- Option to set the VerilogHeader and SystemVerilog file types inside a list file.
- User IP repository are properly tracked by Hog, and a version number is assigned for each used user IP repository in the project.
- Improved automatic GitLab release, with resource utilization table and MR description.
- Option to shrink to size of the automatic messages in the Merge Request page
- New property for tcl scripts inside a list file, to source them.
- Support to IP/BD generated after the creation of the project by tcl scripts (Vivado only).
- New simulation section inside the `hog.conf` file, to setup the simulation property globally or individually for each simulation set in the project.
- Improved support to Quartus.
- Removed support to `proj.tcl` file. Only `hog.conf` can be used to create the projects.

## Hog2021.2  

Main new feature:
- New project file hog.conf
- Support for all simulators supported by Vivado (including Riviera-PRO)
- Simulation library path is now retrieved from environmental variable HOG_SIMULATION_LIB_PATH, if this does not exist, path is set to default SimulationLib inside the top repository folder. An option is added to LaunchWorkflow and CreateProject scripts to overwrite this setting, and set the library path to a custom location (-l/-lib).
- MR pipeline is interruptible, if a newer one starts the old one is cancelled.
- Adding option to configure user ip_repo_paths in the project config file (hog.conf)
- New Vivado buttons, to recreate and check list and project file. Launch Hog/Init.sh to install them
- Support for subfolders inside `Top/`
- FPGA property is now called PART
- max threads are now specified in the hog.conf file in the section [parameters] property name MAX_THREADS
- add pre-creation.tcl and post-creation.tcl scripts, to be executed before or after the project creation. To be stored in the project Top folder

Full changelog available at [gitlab.cern.ch/hog/Hog/-/releases/Hog2021.2](https://gitlab.cern.ch/hog/Hog/-/releases/Hog2021.2)

## Hog2021.1 

- custom files are reset before each stage
- Stages run on a on_success rule
- remove WARNING about pre and post scripts not belonging to utils_1
- Updating copyright, printing the logo
- Allowing separate building branches (HOG_INTERMEDIATE_BRANCH)
- [bugfix] critical warning given for top name missing if there is flavour
- Locked property for IP in list files
- Synthesised IP products in the repository have always the priority to EOS copies
- add HOG_RESET_FILES variable. All the files or pattern listed here will be reset by Hog-CI before starting synthesis
- create wrapper for sigasi CSV
- create make_sigasi_csv.tcl script
- Improvements to pre/post bitstream scripts
- CI is enabled also when Hog is modified
- TagRepository searches now for the greater tag reachable in the branch and not the newest
- create CopyXML shell wrapper
- bugfix: removed hog version from list of fw versions
- (bugfix) Remove -dirty suffix from SHA to fix missing bitfiles from release in case of dirty bin/bit files
- create Execute, ExecuteRet, Git, GitRet functions to handle git commands and shell commands
- GetArtifactsAndRename gets always the artifacts from the last job in the MR pipeline
- Three stages that can be customised by users (user_pre, user_proj, user_post).
- remove submodules from templates
- remove submodules from GetRepoVersions and update its usage everywhere
- new function GetSubmodule returns the submodule name of a given file or path
- MR messages can be configured with the HOG_MR_MSG environmental variable
- Attaching version and timing to the MR
- Note written at collect_artifact stage
