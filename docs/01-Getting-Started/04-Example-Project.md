# Example GitLab Project

An example Hog-handled repository is available at [https://gitlab.com/hog-cern/hog-examples](https://gitlab.com/hog-cern/hog-examples). 

## Clone the Repository

To clone the repository, run the following command in a terminal window.

```bash
mkdir Workdir
cd Workdir
git clone --recursive https://gitlab.com/hog-cern/hog-examples.git
```

## Repository Content

```console
~/Work/Hog/hog-examples/ [main] tree -L 1
.
├── BD                      # Vivado Block Design Folder
├── Hog                     # Hog Submodule
├── IP                      # Vivado/Quartus IP Folder
├── IP_repository           # Vivado User IP Repository
├── README.md               # README file
├── Top                     # Hog Top Project Folder
├── example_max10           # Quartus HDL sources
├── sources                 # Vivado/Libero HDL sources
├── .gitlab-ci.yml          # GitLab CI/CD Configuration 
├── .gitignore              # Git ignore file 
├── .gitmodules             # Git submodule list 
└── vpk120                  # Versal HDL sources

```

The repository contains several example Hog Projects for the different IDEs, and it is already set-up to run the Hog-CI. Inside the `Top` folder, the projects are grouped by IDE.

```console
~/Work/Hog/hog-examples/ [main] tree -L 1 Top/
Top/
├── ise                    # ISE/PlanAhead Projects
├── libero                 # Libero Projects
├── quartus                # Quartus Projects
├── repo.conf              # Hog Repository Configuration File
└── vivado                 # Vivado Projects
```

### Vivado Projects

```console
~/Work/Hog/hog-examples/ [main] tree -L 1 Top/vivado
Top/vivado
├── abacus      # Digilent Basys3 Abacus Project https://github.com/Digilent/Basys-3-Abacus/tree/master
├── bd_design   # Project containing a block design (included with a .bd file)
├── bd_tcl      # Project containing a block design that is created using the BD/bd.tcl script.
├── fifo        # Project containing an IP block (.xci)
├── microblaze  # Project containing a block design with a microblaze processor, that is created using the user post-creation.tcl script.
├── proj.1      # Same as the fifo project, exploiting the Hog Flavour feature
├── proj.2      # Same as proj.1, with flavour number set to 2
└── vpk120      # Example Versal project
```

To run the projects, you need to have Vivado 2022.2 available in your path. If you wish, to run another Vivado version, simply modify the first line of the corresponding `Top/vivado/<proj>/hog.conf` file.

Explore the content of each project, to well understand how Hog handles the different source files. To create a Vivado project, e.g. for the `abacus` project, run from the main path of your repository,

```console
./Hog/Do CREATE vivado/abacus
```

This will create a folder `Projects/vivado/abacus`, where the Vivado project `abacus.xpr` will be created. You can then open the project with the Vivado GUI, or launching the synthesis and implementation workflow with the `Do` command,

```console
./Hog/Do WORKFLOW vivado/abacus
```

The same is valid for all other projects. Please, have a look at the Hog console logs, to understand better what Hog is doing. 

### Quartus Project

```console
~/Work/Hog/hog-examples/ [main] tree -L 1 Top/quartus
Top/quartus
└── example_max10 # Project containing a simple counter, a PLL .qip file and .qsys files
```

To run the project, you need Quartus 23.1 available in your path. To create and build the project, run,

```console
./Hog/Do CREATE quartus/example_max10
./Hog/Do WORKFLOW quartus/example_max10
```

Once you created the project, you can also open it using the Quartus GUI. The project is stored in `Projects/quartus/example_max10/example_max10.qpf`.

### Libero Project


```console
~/Work/Hog/hog-examples/ [main] tree -L 1 Top/libero
Top/libero
└── example_libero # Project containing a simple adder, and FIFO IP created with the post-creation.tcl script
```

To run the project, you need Libero SoC 2022.2.0.10 available in your path. To create and build the project, run,

```console
./Hog/Do CREATE libero/example_libero
./Hog/Do WORKFLOW libero/example_libero
```

Once you created the project, you can also open it using the Quartus GUI. The project is stored in `Projects/libero/example_libero/example_libero.prjx`.


### ISE/PlanAhead Project


```console
~/Work/Hog/hog-examples/ [main*] tree -L 1 Top/ise
Top/ise
└── example_planAhead
```

To run the project, you need ISE 14 available in your path. To create and build the project, run,

```console
./Hog/Do CREATE ise/example_planAhead
./Hog/Do WORKFLOW ise/example_planAhead
```

Once you created the project, you can also open it using the ISE GUI. The project is stored in `Projects/ise/example_planAhead`.

## Continuous Integration

The Hog-CI is setup for the project with static pipelines. Have a look at the [`.gitlab-ci.yml`](https://gitlab.com/hog-cern/hog-examples/-/blob/main/.gitlab-ci.yml?ref_type=heads) to check how it is configured. This main configuration file includes the individual CI configuration yaml for the projects to be run. For instance:

```yaml
include:
  - local: 'Top/vivado/fifo/gitlab-ci.yml'
```

will include the configuration for the `vivado/fifo` project. Looking at each individual configuration file, you can see which jobs will be run. For example, still for the `vivado/fifo` project, we have:

```yaml
GEN:vivado/fifo:
    extends: .generate_project
    variables:
      extends: .vars
      PROJECT_NAME: vivado/fifo
      
SIM:vivado/fifo:
    extends: .simulate_project
    variables:
      extends: .vars
      PROJECT_NAME: vivado/fifo      
```

The main `.gitlab-ci.yml` contains also some global variable settings, which are propagated to each stage. These variables can be then be overwritten in the `variables` section of each job configuration. 

:::{note}
Variables set in the GitLab CI/CD settings of the repository cannot be overwritten in the YAML configurations. 
::: 

That results in the build and simulation jobs to be run in the CI. An example pipeline can be then seen at [this link](https://gitlab.com/hog-cern/hog-examples/-/pipelines/1342176947).

### Dynamic CI

A MR request is open in the repository, which configures the Hog-CI using the dynamic GitLab CI/CD feature. Have a look, at the updated [.gitlab-ci.yml](), to see the different and simpler configuration.





