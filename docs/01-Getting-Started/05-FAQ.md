# Frequently Asked Questions (FAQs)

### Does Hog support HLS in Vivado?
Our idea is to store in the HDL repository only the files required to build the project. Therefore, for HLS, we suggest keeping the C++ code in another repo, and then commit to the HDL repository the resulting user IPs and the `.xci` file. For more details on how to work with user IPs in Vivado, have a look at the [User IP Chapter](../02-User-Manual/01-Hog-local/12-User-IPs.md).

### Does Hog support IPs and Block Design in Vivado?
Sure! Our suggestion is to commit only the `.xci` or `.bd` files to the repository, and add them to your list files. However, many developers prefer to generate their IPs or Block Designs with tcl commands. In this case, just generate them in the `post-creation.tcl` script. For more details, visit the [IP Chapter](../02-User-Manual/01-Hog-local/11-IPs.md).

### What are the differences between Hog and other similar tools like HDLmake or IPBB?
The most important difference is that Hog automatically checks your repository before launching the synthesis on your machine.
And embeds the git SHA and version of your project into the binary file. This means that it is really difficult to make a mistake and produce a binary file that does not correspond to something that is committed to the repository.

If you touch anything in the Vivado project after having created it with the Hog script (modify a file, change a property, etc.), when you launch the synthesis Hog will find out and give you a Critical Warning.

So every bitfile is certified, even if produced locally. I don't think any other tool around does that.
Another important difference is that you don't need to install anything, really nothing. You just need Vivado and git (that you would already need), that's it. Other tools like HDLmake or IPBB need python, specific versions, specific modules, and when you have several developers, it's painful to get them up to speed. Hog is thought to be fast and simple, clone, run the script to create the project, work with Vivado.

### Does Hog support GitHub actions?
Yes, from release 2023.1.

### Why the binary file in my CI is dirty?
Most of the times this is some file that vivado modifies before starting the workflow, so it can be 2 things: `.xci` (IP) or `.bd` (board design)

To find out, open log of the CI (the raw if it's too long) and look for the following line
```
------------------------- PRE SYNTHESIS -------------------------
```

Just after that, cross check that the version in `0.0.0`, that is what Hog does when the repository is dirty.
About 10 or 20 lines above there are some Critical Warnings like these:
```
CRITICAL WARNING: [Hog:Msg-0] Repository is not clean, will u...
```

just above that:

```
WARNING: [Hog:Msg-0] Found non committed changes:... 
 ...<filename>[.bd|xci]            | 4 ++--
 1 file changed, 2 insertions(+), 2 deletions(-) 
```

that's our culprit, Vivado modified this file before starting the workflow.
Now let's understand what Vivado has modified in the file and, especially, if it's going happen all the times or it just happened once. 

To do that, we can open the `diff_pre_synthesis.txt` file in the artefacts, in the file you will find something like:

```diff
diff --git a<filename> b<filename>
index 54542ad6..51ff68ad 100644
--- a<filename>
+++ b<filename>
@@ -3485,8 +3485,8 @@
       "proc_sys_reset_1": {
         "vlnv": "xilinx.com:ip:proc_sys_reset:5.0",
         "ip_revision": "14",
-        "xci_name": "vpk180_ipb_proc_sys_reset_0_1",
-        "xci_path": "ip/vpk180_ipb_proc_sys_reset_0_1/vpk180_ipb_proc_sys_reset_0_1.xci",
+        "xci_name": "vpk180_ipb_proc_sys_reset_1_0",
+        "xci_path": "ip/vpk180_ipb_proc_sys_reset_1_0/vpk180_ipb_proc_sys_reset_1_0.xci",
         "inst_hier_path": "proc_sys_reset_1"
       }
     },
```

In this case, Vivado decided to swap `1_0` with `0_1`. In general it can change some meaningless number or write things in a different order.

You can try to solve this in several ways:

- Clean the repository locally, for example `git clean -xdf`, restart the complete workflow and see if the file gets modified in the same way. If it does, commit the changes.
- Manually do exactly the same change locally and commit it.
- For the `.bd` files you can use the Hog_CI's option to automatically reset the board-design files.

